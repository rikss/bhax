#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Window w(-2, 2, -2, 2, 1000, 255);

    w.show();
    return a.exec();
}
