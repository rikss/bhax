#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImage>
#include <QPainter>
#include <QPixmap>
#include <QWidget>
#include <QMouseEvent>
#include "halmaz.h"

class Halmaz;
class Window : public QMainWindow
{
    Q_OBJECT

public:
    Window(_Float64  a = -2.0, _Float64  b = .7, _Float64  c = -1.35,
           _Float64  d = 1.35, int width = 1000,
           int maxIterations = 255, QWidget *parent = 0);
    ~Window();
    QImage * image;
    Halmaz * mandelbrot;
    void paintEvent(QPaintEvent*);
    void showRow(int height , int * row, int size, int maxIterations) ;
    void mousePressEvent(QMouseEvent*);
    void mouseMoveEvent(QMouseEvent*);
    void mouseReleaseEvent(QMouseEvent*);
    void keyPressEvent(QKeyEvent*);
    bool running;
    int x, y, rectx, recty, maxIterations, width, height;
    _Float64 a, b, c, d;


};
#endif // MAINWINDOW_H
