#include "halmaz.h"
#include <QtDebug>
Halmaz::Halmaz(double a, double b, double c, double d, int width, int height, int maxIterations, Window *window)
{
    this-> a = a;
    this-> b = b;
    this-> c = c;
    this->d = d;
    this-> width = width;
    this-> height = height;
    this-> maxIterations = maxIterations;
    this-> window = window;
    row = new int[width];
    qDebug()<<width<<" "<<height<<" "<<maxIterations<<"\n";
    delete window->image;
    window->image = new QImage(width, height, QImage::Format_RGB32);
    window->setFixedSize(width, height);
}
Halmaz::~Halmaz()
{
    delete[] row;
}


void Halmaz::run()
{
    window->running = true;
    double dx = (b-a)/width;
    double dy = (d-c)/height;
    double reC, imC, reZ, imZ, ujreZ, ujimZ;
    int iteracio = 0;
    for(int j=0; j<height; ++j) {
        //sor = j;
        for(int k=0; k<width; ++k) {
            reC = a+k*dx;
            imC = d-j*dy;
            reZ = 0;
            imZ = 0;
            iteracio = 0;
            while(reZ*reZ + imZ*imZ < 4 && iteracio < maxIterations)
            {
                ujreZ = reZ*reZ - imZ*imZ + reC;
                ujimZ = 2*reZ*imZ + imC;
                reZ = ujreZ;
                imZ = ujimZ;
                ++iteracio;
            }
            row[k] = iteracio;
        }
        window->showRow(j, row, width, maxIterations);
    }
    window->running = false;
    qDebug()<<" ready!";
}
