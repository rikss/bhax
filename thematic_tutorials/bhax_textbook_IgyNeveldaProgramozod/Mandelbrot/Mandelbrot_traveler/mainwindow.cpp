#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtDebug>
#include <iostream>
Window :: Window(_Float64  a, _Float64  b, _Float64  c, _Float64  d,
                 int width, int maxIterations, QWidget *parent)
    : QMainWindow(parent)

{
    this -> height = (int)(width * ((d-c)/(b-a)));
    this -> width = width;
    x = y = rectx = recty = 0;
    this->setFixedSize(width, height);
    this->a = a;
    this->b = b;
    this->c = c;
    this->d = d;
    this->maxIterations = maxIterations;
    image = new QImage(width, height, QImage::Format_RGB32);
    mandelbrot = new Halmaz(a, b, c, d, width, height, maxIterations, this);
    mandelbrot->run();

}

Window::~Window()
{
    delete image;
    delete mandelbrot;
}

void Window::paintEvent(QPaintEvent*) {
    QPainter qpainter(this);
    qpainter.drawImage(0, 0, *image);
    if(!running) {
        qpainter.setPen(QPen(Qt::green, 1));
        qpainter.drawRect(x, y, rectx, recty);
    }
    qpainter.end();

}
void Window::showRow(int height , int * row, int size, int maxIterations)
{
    for(int i=0; i<size; ++i) {
        QRgb szin;
        if(row[i] == maxIterations)
            szin = qRgb(0,0,0);
        else
            szin = qRgb(
                        255-row[i],
                        255-row[i]%64,
                        255-row[i]%32 );
        image->setPixel(i, height, szin);
    }
    update();
}
void Window::mousePressEvent(QMouseEvent* e) {

    x = e->x();
    y = e->y();
    rectx = 0;
    recty = 0;
    qDebug()<<x<<" "<<y;
    update();
}

void Window::mouseMoveEvent(QMouseEvent* e) {

    rectx = e->x() - x;
    recty = e->y() - y;
    update();
}

void Window::mouseReleaseEvent(QMouseEvent* e) {

    if(running) return;
    running = true;
    qDebug()<<e->x()<<" "<<e->y();
    qDebug()<<x+rectx<<" "<<y+recty;

    _Float64  dx = (b-a)/width;
    _Float64  dy = (d-c)/height;

    _Float64 A = (x-width/2)*(b-a)/width;
    qDebug()<<QString::number(A, 'f', 10);
    a = (x-width/2)*dx;
    b = (x+rectx-width/2)*dx;
    d = (y-height/2)*dy;
    c = (y+recty-height/2)*dy;

    qDebug()<<a<<" "<<b<<" "<<c<<" "<<d;
    delete mandelbrot;
    mandelbrot = new Halmaz(a, b, c, d, width, height, maxIterations, this);
    mandelbrot->run();

    update();
}

void Window::keyPressEvent(QKeyEvent *e)
{

    if(running)
        return;
    running = true;
    if (e->key() == Qt::Key_N)
        maxIterations *= 2;
    if (e->key() == Qt::Key_Escape)
    {
        a = -2;
        b = 2;
        c = -2;
        d = 2;
        width = 500;
        height = 500;
        maxIterations = 1000;
    }
    delete mandelbrot;
    mandelbrot = new Halmaz(a, b, c, d, width , height, maxIterations, this);
    mandelbrot->run();

}


