#ifndef HALMAZ_H
#define HALMAZ_H
#include <QThread>
#include <QImage>
#include "mainwindow.h"
class Window;
class Halmaz : public QThread
{
    Q_OBJECT

public:
    Halmaz(double a, double b, double c, double d,
             int width, int height, int maxIterations, Window* window);
    ~Halmaz();
    void run();

    double a, b, c, d;
    int width, height;
    int maxIterations;
    Window* window;
    int* row;
};

#endif // HALMAZ_H
