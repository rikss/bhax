#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int
main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();
    int x = 0, y = 0, xnov = 1, ynov = 1, maxX, maxY;
    for ( ;; ) {
        getmaxyx ( ablak, maxY , maxX );
        mvprintw ( y, x, "o" );
        refresh ();
        usleep ( 50000 );
        clear();
        x = x + xnov;
        y = y + ynov;
        if ( x>=maxX-1 ) xnov = xnov * -1;/*elerte-e a jobb oldalt?*/
        if ( x<=0 )      xnov = xnov * -1;/*elerte-e a bal oldalt? */
        if ( y<=0 )      ynov = ynov * -1;/*elerte-e a tetejet?*/ 
        if ( y>=maxY-1 ) ynov = ynov * -1;/* elerte-e a aljat?*/
    }
    return 0;
}