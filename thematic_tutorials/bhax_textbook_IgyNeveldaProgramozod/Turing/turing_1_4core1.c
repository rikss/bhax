/*#include <stdbool.h>
#include <pthread.h>
//forrás a https://gribblelab.org/CBootCamp/A2_Parallel_Programming_in_C.html
void *cpukiller()
{
  while(true);
  return NULL;
}

int main()
{
  int reference; //????
  pthread_t szalak[4];
  for (int i=0; i<4; i++)
  {
    pthread_create(&szalak[i], NULL, cpukiller, NULL);
  }
  for (int i= 0; i<4; i++)
  {
    pthread_join(szalak[i], NULL);
  }
  return 1;
}*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define NTHREADS 5

void *myFun(void *x)
{
  int tid;
  tid = *((int *) x);
  printf("Hi from thread %d!\n", tid);
  return NULL;
}

int main(int argc, char *argv[])
{
  pthread_t threads[NTHREADS];
  int thread_args[NTHREADS];
  int rc, i;

  /* spawn the threads */
  for (i=0; i<NTHREADS; ++i)
    {
      thread_args[i] = i;
      printf("spawning thread %d\n", i);
      rc = pthread_create(&threads[i], NULL, myFun, (void *) &thread_args[i]);
    }

  /* wait for threads to finish */
  for (i=0; i<NTHREADS; ++i) {
    rc = pthread_join(threads[i], NULL);
  }

  return 1;
}