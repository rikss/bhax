#include <stdio.h>

int main()
{
    //eredeti állapot
    int a = 7;
    int b = 5;

    //std::cout << "a =" << a << " b =" << b << "\n";
    printf("a = %i b = %i\n", a, b);

    //változócsere

    int csere = a;
    a = b;
    b = csere;

    //std::cout << "a=" << a << " b=" << b << std::endl;
    printf("a = %i b = %i\n", a, b);

    //visszacsere

    b += a;
    a = b-a;
    b = b-a;

    //std::cout << "a =" << a << " b =" << b << "\n";
    printf("a = %i b = %i\n", a, b);

    return 0;
}