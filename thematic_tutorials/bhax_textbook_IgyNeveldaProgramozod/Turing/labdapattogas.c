#include <stdio.h>
#include <curses.h>
#include <unistd.h>
#include <math.h>

int
signum (int a)
{
    float af = (float)a;
    return (signbit(af)>>31)*2+1;
}
int
main ( void ){
    WINDOW*ablak;
    ablak = initscr ();
    int x = 0, y = 0, xnov = 1, ynov = 1, maxX, maxY;
    for ( ;; ) {
        getmaxyx ( ablak, maxY , maxX );
        mvprintw ( y, x, "o" );
        refresh ();
        usleep ( 100000 );
        clear();
        xnov *= signum(x+xnov);
        ynov *= signum(y+ynov);
        xnov *= (signum((x+xnov)/maxX-1)*-1);
        ynov *= (signum((y+ynov)/maxY-1)*-1);
        x = x + xnov;
        y = y + ynov;
    }
    return 0;
}
