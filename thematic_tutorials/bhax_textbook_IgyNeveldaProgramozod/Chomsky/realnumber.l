%{
#include <stdio.h>
int realnumbers = 0;
int negative = 0;
%}
digit	[0-9]
%%
{digit}*(\.{digit}+)?	{
    ++realnumbers; 
    float num =  atof(yytext);
    if (negative==1 && num !=0) 
        {
            num = -num;
            printf("[realnum=-%s %f]", yytext, num);
        }
    else 
    {printf("[realnum=%s %f]", yytext, num);}
    negative = 0;
    }
\- {negative = 1;}
%%
int
main ()
{
 yylex ();
 printf("The number of real numbers is %d\n", realnumbers);
 return 0;
}
