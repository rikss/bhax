#include <stdio.h>
#include <stdlib.h>
int main()
{
    int nr = 5;
    double **Mptr; 
    if ((Mptr = (double **) malloc (nr * sizeof(double *))) == NULL)
    {
        return -1;
    }
    for (int i = 0; i < nr; i++){
        if ((Mptr[i]=(double *)malloc((i+1) * sizeof(double)))==NULL)
        {
            return -1;
        }
    }
    for (int i = 0; i <nr; i++){
        for (int j = 0; j <i+1;j++){
            Mptr[i][j]=0;
        }
    }
    
    Mptr[3][0] = 42.0;
    *(Mptr + 3)[1] = 43.0; // mi van, ha itt hiányzik a külső ()
    *(Mptr[3] + 2) = 44.0;
    *(*(Mptr + 3) + 3) = 45.0;

    for (int i = 0; i < nr; i++){
        for ( int j = 0; j <i+1; j++){
            printf("%f ",Mptr[i][j]);
        }
        printf("\n");
    }

    for (int i = 0; i < nr;){
        free(Mptr[i]);
    }
    free(Mptr);
    return 0;
}